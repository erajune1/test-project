/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe('Test', () => {
  beforeEach(() => {
    cy.visit('https://static-canibuild-web.nonprod.cloud.verumtek.com.au/home')
  });
  it('Test', () => {
    cy.log(Cypress.env('username'));
    cy.get('#username').clear().type(Cypress.env('username')).should('have.value', Cypress.env('username'));
  });
  it('Test password', () => {
    //const password = $APP_SECRET
    cy.get('#username').clear().type(Cypress.env('username')).should('have.value', Cypress.env('username'));
    cy.get('#password').clear().type(Cypress.env('password'));
    cy.contains('Sign in').click();
  });
});
